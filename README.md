# Word Correlations #

Shiny application for exploring correlation among word occurrences over the years. 

For simpler installation and running instructions see [wordcor on github](https://github.com/suppechasper/wordcor).

## Installation ##

For simpler installation and running instructions see [wordcor on github](https://github.com/suppechasper/wordcor).

- Install R
- Run R and execute:
    - install.packages( c("shiny", "DT", "Matrix", "KernSmooth", "plotrix", "Rtsne", "devtools") )
    - devtools::install_github("suppechasper/gmra")
- Download the app https://bitbucket.org/suppechasper/wordcor/downloads and unzip

## Running ##

For simpler installation and running instructions see [wordcor on github](https://github.com/suppechasper/wordcor).

- Start R in downloaded folder that contains server.R and ui.R and execute:
     - library( shiny )
     - runApp()

Every time a different smoothing level is selected it will be stored permanently and will in the future not have to be recomputed. Thus, after the first time it should start up quicker.

### Dependencies ###

- General R packages: shiny, DT, plotrix, Rtsne, Kernsmooth
     - additionally jsonlite if data needs to be prepared
- Personal R packages: gmra ( https://bitbucket.org/suppechasper/gmra )