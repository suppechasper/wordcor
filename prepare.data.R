library( jsonlite )
library( Matrix )


files <- dir(path="~/Research/Data/nouns/", pattern="*.json", full.names = TRUE)

ii <- c()
jj <- c()
x <- c()
xm <- c()
xs <- c()

rowIndex <- 0
allwords <- c()
for(f in files){
  print(f)
  
  json <- fromJSON( f )
  words <- tolower(json$word)
  words <- gsub("_", "", words)
  uwords <- unique( words )

  n <- length(words)
  for(i in 1:n ){
    row <- rowIndex + which( uwords == words[i] )
    ii <- c(ii, rep(row, length(json$years[[i]]) ) )
    jj <- c(jj, as.integer(json$years[[i]]) )
    tmp <- as.double(json$counts[[i]])
    x <- c(x, tmp )
  }

  allwords <- c(allwords, uwords)
  rowIndex <- rowIndex + length(uwords)

}

minYear = min(jj)
maxYear = max(jj)
jj <- jj - minYear + 1

data <- list()
data$raw <- sparseMatrix(i = ii, j = jj, x = x)
rownames(data$raw) <- allwords


data$words <- allwords

save.image(data, minYear, maxYear, "google-nouns.Rdata")
